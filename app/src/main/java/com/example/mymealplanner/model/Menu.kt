package com.example.mymealplanner.model

import androidx.room.*
import java.io.Serializable




@Entity (tableName = "menu", indices = [Index(value = ["id"], unique = true)])
data class Menu(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var name: String): Serializable







