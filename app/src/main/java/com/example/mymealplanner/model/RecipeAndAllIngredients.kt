package com.example.mymealplanner.model

import androidx.room.Embedded
import androidx.room.Relation

class RecipeAndAllIngredients {
    @Embedded
    var recipe:RecipeDetail ?= null

   @Relation(parentColumn = "id",entityColumn = "recipeId")
    var ingredients: List<ExtendedIngredient>?= ArrayList()
}