package com.example.mymealplanner.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "extended_ingredients",
        foreignKeys = [ForeignKey(entity = RecipeDetail::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("recipeId"),onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE
        )])
data class ExtendedIngredient(
    @PrimaryKey(autoGenerate = false)
    val id : Int?=0,
    val recipeId: Int? =0,
    @SerializedName("name")
    val name: String? =""

)