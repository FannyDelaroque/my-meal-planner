package com.example.mymealplanner.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
sealed class Item{

@Entity(tableName = "breakfast_item",
    foreignKeys = [ForeignKey(entity = Menu::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("menuId"), onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE
    )]
)

class BreakfastItem(@PrimaryKey (autoGenerate = true)
                    @ColumnInfo(name = "breakfast_id")
                    var id: Int,
                    var menuId: Int,
                    var name: String): Item() {

    constructor() : this(0, 0, "")
}

    @Entity(
        tableName = "lunch_item",
        foreignKeys = [ForeignKey(
            entity = Menu::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("menuId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )]
    )
    data class LunchItem(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "lunch_id")
        var id: Int,
        var menuId: Int,
        var name: String
    ) : Item(){
        constructor() : this(0, 0, "")
    }

    @Entity(tableName = "diner_item",
        foreignKeys = [ForeignKey(entity = Menu::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("menuId"), onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE
        )]
    )
    data class DinerItem (
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "diner_id")
        var id: Int,
        var menuId: Int,
        var name: String): Item(){
        constructor() : this(0, 0, "")
    }
}