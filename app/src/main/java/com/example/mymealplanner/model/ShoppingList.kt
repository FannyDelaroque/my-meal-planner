package com.example.mymealplanner.model

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "shopping_list")
data class ShoppingList(
    @PrimaryKey(autoGenerate = true)
    var id : Int,
    var name:String,
    var quantity: Int) {
}