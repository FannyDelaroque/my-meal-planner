package com.example.mymealplanner.model

import com.google.gson.annotations.SerializedName

data class RecipeList (
    @SerializedName("results")
    val result: List<Recipe>)



