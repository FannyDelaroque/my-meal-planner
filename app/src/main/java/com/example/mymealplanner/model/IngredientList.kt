package com.example.mymealplanner.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class IngredientList (
    @SerializedName("extendedIngredients")
    val ingredients: List<ExtendedIngredient>,
    @SerializedName("id")
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("readyInMinutes")
    val readyInMinutes: Int,
    @SerializedName("servings")
    val servings: Int,
    @SerializedName("sourceUrl")
    val sourceUrl: String ,
    @SerializedName("title")
    var title: String,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("instructions")
    val instructions: String,
    @SerializedName("healthScore")
    val healthScore: Int)
