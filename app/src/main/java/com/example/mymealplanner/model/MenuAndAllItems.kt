package com.example.mymealplanner.model

import androidx.room.Embedded
import androidx.room.Relation


class MenuAndAllItems {
    @Embedded
    var menu: Menu? = null

    @Relation(parentColumn = "id",entityColumn = "menuId")
    var breakfastItems: List<Item.BreakfastItem> = ArrayList()
    @Relation(parentColumn = "id",entityColumn = "menuId")
    var lunchItems: List<Item.LunchItem> = ArrayList()
    @Relation(parentColumn = "id",entityColumn = "menuId")
    var dinerItems: List<Item.DinerItem> = ArrayList()
}