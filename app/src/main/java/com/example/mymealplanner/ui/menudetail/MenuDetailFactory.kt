package com.example.mymealplanner.ui.menudetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class MenuDetailFactory(private  val  menuId: Int): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MenuDetailViewModel(menuId) as T
    }
}