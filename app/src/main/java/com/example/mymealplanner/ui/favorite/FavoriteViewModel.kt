package com.example.mymealplanner.ui.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.model.RecipeAndAllIngredients
import com.example.mymealplanner.model.RecipeDetail
import java.util.concurrent.Executors

class FavoriteViewModel: ViewModel() {
    val favorite: LiveData<List<RecipeDetail>> = App.database.favoriteDao().getAllFavorites()

    fun deleteToFavorite(recipe: RecipeDetail) {
        Executors.newSingleThreadExecutor().execute {
            App.database.favoriteDao().deleteFavorite(recipe)
        }
    }
}