package com.example.mymealplanner.ui.recipelist

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.helpers.Connexion
import com.example.mymealplanner.helpers.ResultCallback
import com.example.mymealplanner.model.Recipe
import com.example.mymealplanner.model.RecipeList
import com.example.mymealplanner.ui.recipedetail.RecipeActivity
import kotlinx.android.synthetic.main.fragment_recipe_list.*


/**
 * A simple [Fragment] subclass.
 */
class RecipeListFragment : Fragment(), RecipeListAdapter.RecipeListAdapterListener,
    androidx.appcompat.widget.SearchView.OnQueryTextListener,
    MenuItem.OnActionExpandListener, ResultCallback {

    lateinit var viewModel: RecipeListViewModel
    lateinit var recipeList : MutableList<Recipe>
    private var recyclerView: RecyclerView? = null
    private lateinit var adapter: RecipeListAdapter
    lateinit var searchView: androidx.appcompat.widget.SearchView
    lateinit var searchItem: MenuItem


    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater.inflate(R.menu.home, menu)
        searchItem = menu?.findItem(R.id.action_settings)
        searchView = searchItem?.actionView as androidx.appcompat.widget.SearchView
        searchView.setOnQueryTextListener(this)
        searchItem.setOnActionExpandListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(RecipeListViewModel::class.java)
        viewModel.loadRecipe()
        viewModel.recipeList.observe(viewLifecycleOwner, Observer { recipes-> display(recipes) })
        viewModel.getConnexion().observe(viewLifecycleOwner, Observer { connexion -> monitor(connexion) })

    }

    private fun monitor(connexion: Connexion) {
        when (connexion) {
            Connexion.IS_OK -> onSuccess()
            Connexion.NOT_OK -> onError()
            Connexion.EN_COURS -> onPrepare()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_recipe_list, container, false)
        activity?.title = "Liste de recettes"

        recipeList = mutableListOf()



        return view
    }

    private fun display(recipes: RecipeList) {
        if (recipes != null){
            recipeList.clear()
            recipeList.addAll(recipes.result)
            adapter = RecipeListAdapter(recipeList, this, requireContext())
            recyclerView = recyclerRecipeList as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this.requireContext())
            recyclerView!!.adapter =adapter
            }


    }

    override fun openRecipeClicked(recipe: Recipe) {
        val intent = Intent(this.context, RecipeActivity::class.java)
        intent.putExtra(RecipeActivity.RECIPE_ID, recipe.id)
        intent.putExtra(RecipeActivity.RECIPE_NAME, recipe.title)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {

        if (query != null) {
            viewModel.loadSearchRecipe(query)
        }
        searchView.clearFocus()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }


    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
       viewModel.loadRecipe()
        return true
    }

    override fun onPrepare() {
        progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess() {
        progressBar.visibility = View.GONE
    }

    override fun onError() {
        progressBar.visibility = View.VISIBLE
    }


}
