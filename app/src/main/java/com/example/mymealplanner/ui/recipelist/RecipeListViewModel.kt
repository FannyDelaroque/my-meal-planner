package com.example.mymealplanner.ui.recipelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.helpers.Connexion
import com.example.mymealplanner.helpers.ResultCallback
import com.example.mymealplanner.model.RecipeList

class RecipeListViewModel():ViewModel(),ResultCallback {

   private val _recipeList: MutableLiveData<RecipeList> = MutableLiveData()
    val recipeList= _recipeList

    val callbacks: ResultCallback = this

    private val connexion = MutableLiveData<Connexion>().apply {

    }
    fun getConnexion() : LiveData<Connexion> = connexion


    fun loadRecipe(){
        App.repository.getRandomRecipes(callbacks){
            recipelist, error ->  _recipeList.value = recipelist
        }
    }

    fun loadSearchRecipe(query:String){
        App.repository.getSearchRecipe(callbacks,query){
             recipelist, error ->  _recipeList.value = recipelist
        }
    }

    override fun onPrepare() {
        connexion.value = Connexion.EN_COURS
    }

    override fun onSuccess() {
        connexion.value = Connexion.IS_OK
    }

    override fun onError() {
        connexion.value = Connexion.NOT_OK
    }


}