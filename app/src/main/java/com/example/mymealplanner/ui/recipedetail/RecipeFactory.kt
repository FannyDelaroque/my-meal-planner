package com.example.mymealplanner.ui.recipedetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mymealplanner.ui.menudetail.MenuDetailViewModel

class RecipeFactory(private  val  recipeId: Int): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RecipeDetailViewModel(recipeId) as T
    }
}