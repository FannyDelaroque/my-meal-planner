package com.example.mymealplanner.ui.shoppinglist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.ShoppingList
import kotlinx.android.synthetic.main.shopping_list.view.*


class ShoppingListAdapter(var shoppingList: List<ShoppingList>, val listener: ShoppingListAdapterListener, var context: Context): RecyclerView.Adapter<ShoppingListAdapter.ShoppingListViewHolder>(){



    interface ShoppingListAdapterListener {
        fun removeSelectecItem(article: ShoppingList)
        fun updateArticleInShoppingList(article: ShoppingList, number: Int)
        fun swapArticle(article: ShoppingList, newArticle:ShoppingList)


    }

    fun removeItem(int: Int, context: Context) {
        listener.removeSelectecItem(shoppingList[int])
    }

    fun updateItem(int: Int,context: Context, number:Int){
        listener.updateArticleInShoppingList(shoppingList[int],number)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListViewHolder {
       val rootView = LayoutInflater.from(parent.context).inflate(R.layout.shopping_list,parent,false)
       val holder = ShoppingListViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
        return shoppingList.size
         }

    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        var article = shoppingList[position]
        if (article!= null){
            holder.fillWithArticle(article)


        }
    }

    fun swapItems(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                val list = shoppingList.toMutableList()
                list[i] = list.set(i+1, list[i]);
            }
        } else {
            for (i in fromPosition..toPosition + 1) {
                val list = shoppingList.toMutableList()
                list[i] = list.set(i-1, list[i]);
            }
        }

        notifyItemMoved(fromPosition, toPosition)
        listener.swapArticle(shoppingList[fromPosition], shoppingList[toPosition])


    }



    inner class ShoppingListViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView),
        View.OnClickListener {

        private val name = rootView.textArticleName
        private val btnQuantityLess = rootView.btnDecrement
        private val btnQuantityMore = rootView.btnIncrement
        private val doneBtn = rootView.imageDone
        private var txtDisplayQuantity = rootView.txtDisplayQuantity
        private val container = rootView.containerArticle
        private var count = 0




        fun fillWithArticle(article: ShoppingList) {
            name.text = article.name
            txtDisplayQuantity.text = article.quantity.toString()
            count = article.quantity

        }

        init {
            doneBtn.setOnClickListener(this)
            btnQuantityLess.setOnClickListener(this)
            btnQuantityMore.setOnClickListener(this)


        }



        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.imageDone -> removeItem(adapterPosition, v.context)
                R.id.btnDecrement -> removeQuantity()
                R.id.btnIncrement -> addQuantity()
            }
        }

        private fun addQuantity() {
            count++
            txtDisplayQuantity.text = count.toString()
            updateItem(adapterPosition,context, count)

        }

        private fun removeQuantity() {
            if(count>0){
                count--
                txtDisplayQuantity.text = count.toString()
                updateItem(adapterPosition, context, count)

            }
        }




    }


}



