package com.example.mymealplanner.ui.recipedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.helpers.Connexion
import com.example.mymealplanner.helpers.ResultCallback
import com.example.mymealplanner.model.IngredientList
import com.example.mymealplanner.model.RecipeAndAllIngredients
import com.example.mymealplanner.model.RecipeDetail
import com.google.gson.annotations.SerializedName
import java.util.concurrent.Executors

class RecipeDetailViewModel(id: Int): ViewModel(), ResultCallback {

    private val _recipe = MutableLiveData<IngredientList>()
    val recipe: LiveData<IngredientList> = _recipe

    private val favoriteIdLiveData = MutableLiveData<Int>()
    val currentRecipe : LiveData<RecipeDetail> = Transformations.switchMap(favoriteIdLiveData){
            id -> App.database.favoriteDao().getFavoritesById(id)
    }

    lateinit var favoriteRecipe:RecipeDetail

    init {
        favoriteIdLiveData.value = id
    }

    val callbacks: ResultCallback = this
    private val connexion = MutableLiveData<Connexion>().apply {
    }
    fun getConnexion() : LiveData<Connexion> = connexion


    fun loadRecipe(id: Int){
        App.repository.getRecipeById(callbacks,id){
            recipe, error -> _recipe.value = recipe
            if(recipe!=null){
                favoriteRecipe = RecipeDetail(recipe.id,recipe.image,recipe.readyInMinutes,recipe.servings,
                    recipe.sourceUrl,recipe.title, recipe.summary,recipe.instructions,recipe.healthScore)

            }
        }
    }

    fun addToFavorite(){
        Executors.newSingleThreadExecutor().execute {
            App.database.favoriteDao().insertFavorite(favoriteRecipe)
        }

    }

    fun deleteToFavorite(){
        Executors.newSingleThreadExecutor().execute {
            App.database.favoriteDao().deleteFavorite(favoriteRecipe)
        }
    }

    override fun onPrepare() {
        connexion.value = Connexion.EN_COURS
    }

    override fun onSuccess() {
        connexion.value = Connexion.IS_OK
    }

    override fun onError() {
        connexion.value = Connexion.NOT_OK
    }
}