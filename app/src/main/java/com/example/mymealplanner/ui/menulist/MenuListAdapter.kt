package com.example.mymealplanner.ui.menulist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.MenuAndAllItems
import kotlinx.android.synthetic.main.menulist.view.*
import java.io.BufferedReader
import java.util.*

class MenuListAdapter( var menuList : List<MenuAndAllItems>, val listener : MenuListListener): RecyclerView.Adapter<MenuListAdapter.MenuListViewHolder>(){

    interface MenuListListener {
        fun onMenuSelected(menu : MenuAndAllItems)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuListViewHolder {
        val rootView = LayoutInflater.from(parent.context).inflate(R.layout.menulist, parent,false)
        val holder = MenuListViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
       return menuList.size
    }

    override fun onBindViewHolder(holder: MenuListViewHolder, position: Int) {
        val menu = menuList[position]
        if(menu != null){
            holder.fillWithMenu(menu)
        }
    }

    fun onMenuClickListener(index:Int,context:Context){
        val menu = menuList[index]
        if (menu != null) {
            listener.onMenuSelected(menu)
        }
    }

    inner class MenuListViewHolder(rootView: View): RecyclerView.ViewHolder(rootView),
    View.OnClickListener{
        private val name = rootView.textViewName
        private val breakfastItem = rootView.textViewBreakfastItem
        private val lunchItem = rootView.textViewLunchItem
        private val dinerItem = rootView.textViewDinerItem

        init {
            rootView.setOnClickListener(this)
        }

        fun fillWithMenu(menu: MenuAndAllItems){
            name.text = menu.menu?.name.toString()

            var breakfastList = menu.breakfastItems
            val lunchList = menu.lunchItems
            val dinerList = menu.dinerItems


            var breakfastBuilder = StringBuilder()
            for( breakfast in  breakfastList){
                if(breakfast.name != ""){
                    breakfastBuilder .append(breakfast.name)
                    breakfastBuilder .append(",")
                }
            }
            if (breakfastBuilder.length > 1){
                breakfastBuilder.setLength(breakfastBuilder.length-1)
            }
            breakfastItem.text = breakfastBuilder

            val lunchBuilder = StringBuilder()
            for(lunch in lunchList){
                if (lunch.name != ""){
                    lunchBuilder.append(lunch.name)
                    lunchBuilder.append(",")
                }
            }
            if (lunchBuilder.length > 1){
                lunchBuilder.setLength(lunchBuilder.length-1)
            }
            lunchItem.text = lunchBuilder

            val dinerBuilder = StringBuilder()
            for(diner in dinerList){
                if (diner.name != ""){
                    dinerBuilder.append(diner.name)
                    dinerBuilder.append(",")
                }
            }
            if (dinerBuilder.length > 1){
                dinerBuilder.setLength(dinerBuilder.length-1)
            }
            dinerItem.text = dinerBuilder

        }

        override fun onClick(view: View?) {
            if (view != null){
                onMenuClickListener(adapterPosition, view.context)
            }
        }
    }

}


