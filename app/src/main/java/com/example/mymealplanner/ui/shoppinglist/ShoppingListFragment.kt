package com.example.mymealplanner.ui.shoppinglist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.helpers.DragManageAdapter
import com.example.mymealplanner.model.ShoppingList
import kotlinx.android.synthetic.main.fragment_shopping_list.*
import kotlinx.android.synthetic.main.fragment_shopping_list.view.*


/**
 * A simple [Fragment] subclass.
 */
class ShoppingListFragment : Fragment(),ShoppingListAdapter.ShoppingListAdapterListener,
    AdapterView.OnItemClickListener{

    private lateinit var viewModel: ShoppingListViewModel
    private lateinit var shoppingList: MutableList<ShoppingList>
    lateinit var adapter: ShoppingListAdapter
    lateinit var recyclerView: RecyclerView
    lateinit var searchView: androidx.appcompat.widget.SearchView
    lateinit var searchItem: MenuItem

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu!!, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item != null){
            when (item.itemId){
                R.id.itemSend->sendShoppingList(shoppingList)
            }
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_shopping_list, container, false)
        activity?.title = "Liste de courses"
        shoppingList = mutableListOf()

        viewModel= ViewModelProvider(this).get(ShoppingListViewModel::class.java)
        viewModel.shoppingList.observe(viewLifecycleOwner, Observer { list -> displayShoppingList(list) })



        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inputArticle.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                val article = ShoppingList(
                    id = 0,
                    name = inputArticle.text.toString(),
                    quantity = 0)
                viewModel.addArticleInShoppingList(article)
                inputArticle.text.clear()
                hideKeyboard(inputArticle)

                true
            } else {
                false
            }
        }
    }

    private fun hideKeyboard(view: View) {
        view?.apply {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    private fun displayShoppingList(list: List<ShoppingList>) {
        shoppingList.clear()
        shoppingList.addAll(list)
        adapter= ShoppingListAdapter(shoppingList,this, requireContext())
        recyclerView = recyclerShoppingList
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView!!.adapter =adapter
        // Setup ItemTouchHelper
        val callback = DragManageAdapter(adapter, requireContext(),
            ItemTouchHelper.UP.or(ItemTouchHelper.DOWN), ItemTouchHelper.LEFT.or(ItemTouchHelper.RIGHT))
        val helper = ItemTouchHelper(callback)
        helper.attachToRecyclerView(recyclerView)




    }


    override fun removeSelectecItem(article: ShoppingList) {
        viewModel.removeFromShoppingList(article)
    }



    override fun updateArticleInShoppingList(article: ShoppingList, number: Int) {
        val articleToUpdate = ShoppingList(
            id = article.id,
            name = article.name,
            quantity = number)
        viewModel.updateArticle(articleToUpdate)

    }

    override fun swapArticle(currentArticle: ShoppingList, newArticle:ShoppingList) {
        val article = ShoppingList(newArticle.id, currentArticle.name,currentArticle.quantity)
        val otherArticle = ShoppingList(currentArticle.id,newArticle.name,newArticle.quantity)
        viewModel.updateArticleId(article)
        viewModel.updateArticleId(otherArticle)

    }


    fun sendShoppingList(list: List<ShoppingList>){
        val sb = StringBuilder()
        for (article in list) {
            sb.append(article.name+ " , quantité: " + article.quantity+ "\n")
        }

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
        startActivity(Intent.createChooser(shareIntent,"Share to:"))
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

}
