package com.example.mymealplanner.ui.recipedetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.helpers.Connexion
import com.example.mymealplanner.helpers.ResultCallback
import com.example.mymealplanner.model.ExtendedIngredient
import com.example.mymealplanner.model.IngredientList
import com.example.mymealplanner.model.RecipeDetail
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_recipe.*
import kotlinx.android.synthetic.main.fragment_recipe_list.*

class RecipeActivity : AppCompatActivity(), ResultCallback {

    lateinit var viewModel:RecipeDetailViewModel
    private var isClicked: Boolean ?= null
    private lateinit var recipeName :String
    lateinit var ingredientList : MutableList<ExtendedIngredient>
    lateinit var collapsingToolbarLayout: CollapsingToolbarLayout


companion object{
    const val RECIPE_ID = "id"
    const val RECIPE_NAME = "name"
}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)

        val recipeId = intent.getIntExtra(RECIPE_ID, 0)

        recipeName = intent.getStringExtra(RECIPE_NAME)

        ingredientList = mutableListOf()
        collapsingToolbarLayout = collapsing as CollapsingToolbarLayout
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        collapsingToolbarLayout.title = recipeName

        val factory = RecipeFactory(recipeId)
        viewModel = ViewModelProvider(this,factory).get(RecipeDetailViewModel::class.java)

        if(recipeId!= null){
            viewModel.loadRecipe(recipeId)
        }

        viewModel.recipe.observe(this, Observer { recipe -> display(recipe) })

        // abonnement aux favoris
        viewModel.currentRecipe.observe(this, Observer { recipe -> update(recipe) })

        viewModel.getConnexion().observe(this, Observer { connexion -> monitor(connexion) })

    }

    private fun monitor(connexion: Connexion) {
        when (connexion) {
            Connexion.IS_OK -> onSuccess()
            Connexion.NOT_OK -> onError()
            Connexion.EN_COURS -> onPrepare()
        }
    }

    private fun update(recipe: RecipeDetail?) {
        if (recipe != null){
            btnFavorite.setImageResource(R.drawable.ic_favorite_black_24dp)
            btnFavorite.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
            isClicked = true
        }

    }

    private fun display(recipe: IngredientList) {
        if (recipe != null){



            Picasso.with(this).load(recipe.image).into(image_recipe)
            name_recipe.text = recipe.title
            recipe_description.text = recipe.instructions
            ingredientList.clear()
            val ingredients = recipe.ingredients
            for (result in ingredients){
                    ingredientList.addAll(listOf(result))

            }
            val adapter = RecipeAdapter(ingredientList)
            val recycler = recycler_ingredients as RecyclerView
            recycler.layoutManager = LinearLayoutManager(this)
            recycler.adapter = adapter
        }
    }

    private fun deleteFromFavorite(){
        viewModel.deleteToFavorite()
        Toast.makeText(this, "$recipeName a bien été retiré de vos favoris", Toast.LENGTH_SHORT).show()
        btnFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        btnFavorite.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
        isClicked = false
    }

    private fun addToFavorite(){
        viewModel.addToFavorite()
        Toast.makeText(this, "$recipeName a bien été ajouté à vos favoris", Toast.LENGTH_SHORT).show()
        btnFavorite.setImageResource(R.drawable.ic_favorite_black_24dp)
        btnFavorite.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
        isClicked = true

    }

    fun manageFavorite(view: View){
        if(isClicked == true){
            deleteFromFavorite()
        } else{
            addToFavorite()
        }

    }

    override fun onPrepare() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun onSuccess() {
        progress_bar.visibility = View.GONE
    }

    override fun onError() {
        progress_bar.visibility = View.VISIBLE
    }
}
