package com.example.mymealplanner.ui.menudetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.model.Item.BreakfastItem
import com.example.mymealplanner.model.Item
import com.example.mymealplanner.model.Menu
import com.example.mymealplanner.model.MenuAndAllItems
import java.util.concurrent.Executors

class MenuDetailViewModel (id: Int): ViewModel(){
    private val menuIdLiveData = MutableLiveData<Int>()
    val currentMenu : LiveData<MenuAndAllItems> = Transformations.switchMap(menuIdLiveData){
            id -> App.database.menuDao().getMenuById(id)
    }


    private val breakfastIdLiveData = MutableLiveData<Item.BreakfastItem>()
    val currentBreakfastItem : LiveData<Item.BreakfastItem> = breakfastIdLiveData


    fun getBreakFastItemById(id : Int): BreakfastItem?{
        App.database.itemDao().getBreakfastItemById(id)
        return breakfastIdLiveData.value
    }


    init {
        menuIdLiveData.value = id
    }



    fun updateMenu(menu: Menu){
        Executors.newSingleThreadExecutor().execute {
            val id = App.database.menuDao().getMenuById(menu.id)
            if (id == null){
                App.database.menuDao().insertMenu(menu)
            } else{
                App.database.menuDao().updateMenu(menu)
            }

        }
    }

    fun updateBreakfastItem(breakfastItem: BreakfastItem){
        Executors.newSingleThreadExecutor().execute {
            val id = App.database.itemDao().getBreakfastItemById(breakfastItem.id)
            if (id == null){
                App.database.itemDao().insertBreakfastItem(breakfastItem)
            } else{
                App.database.itemDao().updateBreakfastItem(breakfastItem)
            }

        }
    }

    fun insertBreakfastItem(breakfastItem: BreakfastItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertBreakfastItem(breakfastItem)
        }
    }

    fun updateLunchItem(lunchItem: Item.LunchItem){
        Executors.newSingleThreadExecutor().execute {
            val id = App.database.itemDao().getLunchItemById(lunchItem.id)
            if (id == null){
                App.database.itemDao().insertLunchItem(lunchItem)
            } else{
                App.database.itemDao().updateLunchItem(lunchItem)
            }

        }
    }

    fun insertLunchItem(lunchItem: Item.LunchItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertLunchItem(lunchItem)
        }
    }

    fun updateDinerItem(dinerItem: Item.DinerItem){
        Executors.newSingleThreadExecutor().execute {
            val id = App.database.itemDao().getDinerItemById(dinerItem.id)
            if (id == null){
                App.database.itemDao().insertDinerItem(dinerItem)
            } else{
                App.database.itemDao().updateDinerItem(dinerItem)
            }

        }
    }

    fun insertDinerItem(dinerItem: Item.DinerItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertDinerItem(dinerItem)
        }
    }

    fun deleteBreakfastItem(breakfastItem: BreakfastItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().deleteBreakfastItem(breakfastItem)
        }
    }

    fun deleteLunchItem(lunchItem: Item.LunchItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().deleteLunchItem(lunchItem)
        }
    }

    fun deleteDinerItem(dinerItem: Item.DinerItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().deleteDinerItem(dinerItem)
        }
    }

}