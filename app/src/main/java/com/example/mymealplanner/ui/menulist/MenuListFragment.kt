package com.example.mymealplanner.ui.menulist

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.mymealplanner.R
import com.example.mymealplanner.model.Item
import com.example.mymealplanner.model.Menu
import com.example.mymealplanner.model.MenuAndAllItems
import com.example.mymealplanner.ui.menudetail.MenuDetailActivity
import kotlinx.android.synthetic.main.fragment_menu_list.*


/**
 * A simple [Fragment] subclass.
 */
class MenuListFragment : Fragment(),MenuListAdapter.MenuListListener {

    private val viewModel: MenuListViewModel by viewModels()
    private var recyclerView: RecyclerView? = null
    private lateinit var menuList: MutableList<MenuAndAllItems>
    private lateinit var adapter: MenuListAdapter
    lateinit var searchView: androidx.appcompat.widget.SearchView
    lateinit var addItem: MenuItem
    lateinit var removeItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.add_day,menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item != null){
            when (item.itemId){
                R.id.itemAdd->addNewDay()
                R.id.itemDelete-> deleteDay()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteDay() {
       viewModel.deleteDay()
    }

    private fun addNewDay() {
        val builder = AlertDialog.Builder(this.requireContext())
        builder.setTitle("Ajouter un jour")
        var editText = EditText(this.requireContext())
        builder.setView(editText)
        builder.setPositiveButton(R.string.enregistrer) { dialog, which ->
            var newDay= Menu(0,editText?.text.toString())
                viewModel.addMenu(newDay)

        }
        builder.setNegativeButton(R.string.annuler) { dialog, which ->
        }
        builder.show()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_menu_list, container, false)
        activity?.title = "Mes menus"
        menuList = mutableListOf()
        viewModel.menus.observe(viewLifecycleOwner, Observer { list -> updateMenus(list) })

        return view
    }

    private fun updateMenus(list: List<MenuAndAllItems>) {
        if (list.isNotEmpty()) {
            menuList.clear()
            menuList.addAll(list)
            adapter = MenuListAdapter(menuList, this)
            recyclerView = recyclerViewMenuList as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this.requireContext())
            recyclerView!!.adapter =adapter

        } else {
            val dayList = arrayOf("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche")
            val id = arrayOf(1,2,3,4,5,6,7)
            var initialMenu: Menu
            for (x in 0 until 7){
                initialMenu = Menu(id = id[x], name = dayList[x])
                viewModel.addMenu(initialMenu)
            }

        }

    }

    override fun onMenuSelected(menu: MenuAndAllItems) {
        val intent = Intent(requireContext(), MenuDetailActivity::class.java)
        intent.putExtra(MenuDetailActivity.ID, menu.menu?.id)
        startActivity(intent)
    }




}



