package com.example.mymealplanner.ui.recipedetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.ExtendedIngredient
import com.example.mymealplanner.model.RecipeAndAllIngredients
import kotlinx.android.synthetic.main.ingredients.*
import kotlinx.android.synthetic.main.ingredients.view.*

class RecipeAdapter (val ingredientsList: MutableList<ExtendedIngredient>):RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
       val rootView = LayoutInflater.from(parent.context).inflate(R.layout.ingredients,parent,false)
        val holder = RecipeViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
        return ingredientsList.size
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val ingredient = ingredientsList[position]
        if(ingredient!= null){
            holder.fillWithIngredients(ingredient)
        }
    }

    inner class RecipeViewHolder(rootView:View): RecyclerView.ViewHolder(rootView){

        private val ingredients = rootView.txtIngredientName

        fun fillWithIngredients(ingredient: ExtendedIngredient){
            ingredients.text = ingredient.name
        }

    }

}