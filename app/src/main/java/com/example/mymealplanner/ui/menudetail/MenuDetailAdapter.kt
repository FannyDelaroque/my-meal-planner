package com.example.mymealplanner.ui.menudetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.persistableBundleOf
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.Item.BreakfastItem
import com.example.mymealplanner.model.Item.DinerItem
import com.example.mymealplanner.model.Item
import com.example.mymealplanner.model.Item.LunchItem
import kotlinx.android.synthetic.main.item.view.*
import kotlin.IllegalArgumentException

class MenuDetailAdapter(var itemList: MutableList<Item>, val listener: MenuDetailAdapterListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val TYPE_BREAKFAST= 0
        private const val TYPE_LUNCH = 1
        private const val TYPE_DINER = 2

    }


    interface MenuDetailAdapterListener{
        fun editBreakfastItemSelected(breakfastItem: BreakfastItem)
        fun deleteBreakfastItemSelected(breakfastItem: BreakfastItem)
        fun editLunchItemSelected(lunchItem: LunchItem)
        fun deleteLunchItemSelected(lunchItem: LunchItem)
        fun editDinerItemSelected(dinerItem: DinerItem)
        fun deleteDinerItemSelected(dinerItem: DinerItem)

    }


    fun editBreakfastItem(index: Int, context: Context){
        listener.editBreakfastItemSelected(itemList[index] as BreakfastItem)
    }

    fun deleteBreaksfastItem(index: Int, context: Context){
        listener.deleteBreakfastItemSelected(itemList[index] as BreakfastItem)
    }

    fun editLunchItem(index: Int, context: Context){
        listener.editLunchItemSelected(itemList[index]as LunchItem)
    }
    fun deleteLunchItem(index: Int, context: Context){
        listener.deleteLunchItemSelected(itemList[index]as LunchItem)
    }

    fun editDinerItem(index: Int, context: Context){
        listener.editDinerItemSelected(itemList[index] as DinerItem)
    }
    fun deleteDinerItem(index: Int, context: Context){
        listener.deleteDinerItemSelected(itemList[index] as DinerItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
       return when(viewType){
            TYPE_BREAKFAST -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent,false)
                val holder = BreakfastItemViewHolder(rootView)
                return holder
            }
           TYPE_LUNCH -> {
               val rootView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent,false)
               val holder = LunchItemViewHolder(rootView)
               return holder
           }
           TYPE_DINER -> {
               val rootView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent,false)
               val holder = DinerItemViewHolder(rootView)
               return holder
           }

           else -> throw IllegalArgumentException("Invalid Type view") as Throwable
       }
           }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int  {
        return when (itemList[position]) {
            is BreakfastItem -> TYPE_BREAKFAST
            is LunchItem -> TYPE_LUNCH
            is DinerItem -> TYPE_DINER
            else -> throw IllegalArgumentException("Invalid type of data: $position")
        }
    }

    override fun onBindViewHolder(holder:RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is BreakfastItemViewHolder ->holder.fillWithBreakfastItem(itemList[position] as BreakfastItem)
            is LunchItemViewHolder ->holder.fillWithLunchItem(itemList[position] as LunchItem)
            is DinerItemViewHolder ->holder.fillWithDinerItem(itemList[position] as DinerItem)
        }




    }

    inner class BreakfastItemViewHolder(rootView:View): RecyclerView.ViewHolder(rootView), View.OnClickListener{
        private val itemName = rootView.textItem
        private val editImage = rootView.imageEdit
        private val deleteImage = rootView.imageDelete

        init {
            editImage.setOnClickListener(this)
            deleteImage.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.imageEdit -> editBreakfastItem(adapterPosition, v.context)
                    R.id.imageDelete-> deleteBreaksfastItem(adapterPosition,v.context)
                }
            }
        }

        fun fillWithBreakfastItem(breakfastItem:BreakfastItem) {
            itemName.text = breakfastItem.name
        }

    }
    inner class LunchItemViewHolder(rootView:View): RecyclerView.ViewHolder(rootView), View.OnClickListener{
        private val itemName = rootView.textItem
        private val editImage = rootView.imageEdit
        private val deleteImage = rootView.imageDelete

        init {
            editImage.setOnClickListener(this)
            deleteImage.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.imageEdit -> editLunchItem(adapterPosition, v.context)
                    R.id.imageDelete-> deleteLunchItem(adapterPosition,v.context)
                }
            }
        }

        fun fillWithLunchItem(lunchItem: LunchItem) {
            itemName.text = lunchItem.name
        }

    }
    inner class DinerItemViewHolder(rootView:View): RecyclerView.ViewHolder(rootView), View.OnClickListener{
        private val itemName = rootView.textItem
        private val editImage = rootView.imageEdit
        private val deleteImage = rootView.imageDelete

        init {
            editImage.setOnClickListener(this)
            deleteImage.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.imageEdit -> editDinerItem(adapterPosition, v.context)
                    R.id.imageDelete-> deleteDinerItem(adapterPosition,v.context)
                }
            }
        }

        fun fillWithDinerItem(dinerItem: DinerItem) {
            itemName.text = dinerItem.name
        }

    }

}