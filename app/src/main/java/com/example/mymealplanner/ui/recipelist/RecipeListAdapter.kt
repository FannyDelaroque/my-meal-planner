package com.example.mymealplanner.ui.recipelist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.Recipe
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recipe_list.view.*

class RecipeListAdapter(var recipeList: List<Recipe>, var listener: RecipeListAdapterListener, var context : Context): RecyclerView.Adapter<RecipeListAdapter.RecipeListViewHolder>() {

    interface RecipeListAdapterListener {
        fun openRecipeClicked(recipe: Recipe)
    }

    fun onRecipeClick(position: Int, context: Context) {
        listener.openRecipeClicked(recipeList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeListViewHolder {
        val rootView = LayoutInflater.from(parent.context).inflate(R.layout.recipe_list, parent,false)
        val holder = RecipeListViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }

    override fun onBindViewHolder(holder: RecipeListViewHolder, position: Int) {
        val recipe = recipeList[position]
        if(recipe!=null){
            holder.fillWithRecipeList(recipe)
        }

    }

    inner class RecipeListViewHolder(rootview: View): RecyclerView.ViewHolder(rootview), View.OnClickListener{

        private val title = rootview.title
        private val image = rootview.image
        private val container = rootview.container_cardview
        private var urlImage = "https://spoonacular.com/recipeImages/"

        fun fillWithRecipeList(recipe: Recipe){
            title.text = recipe.title
            Picasso.with(context).load(urlImage+recipe.image).into(image)
        }

        init {
            container.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v != null) {
                onRecipeClick(adapterPosition,v.context)
            }
        }

    }


}


