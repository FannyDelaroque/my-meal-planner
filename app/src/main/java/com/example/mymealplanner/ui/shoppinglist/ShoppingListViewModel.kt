package com.example.mymealplanner.ui.shoppinglist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.model.ShoppingList
import java.util.concurrent.Executors

class ShoppingListViewModel: ViewModel() {

val shoppingList: LiveData<List<ShoppingList>> = App.database.shoppingDao().loadShoppingList()

    fun removeFromShoppingList(article:ShoppingList){
        Executors.newSingleThreadExecutor().execute {
            App.database.shoppingDao().removeItemFromShoppingList(article)
        }
    }

    fun addArticleInShoppingList(article: ShoppingList){
        Executors.newSingleThreadExecutor().execute {
            App.database.shoppingDao().addItemInShoppingList(article)
        }
    }

    fun updateArticleId(article: ShoppingList){
        Executors.newSingleThreadExecutor().execute {
            App.database.shoppingDao().updateItem(article)
        }
    }

    fun updateArticle(article: ShoppingList){
        Executors.newSingleThreadExecutor().execute {
            val id = App.database.shoppingDao().getArticleById(article.id)
            if (id == null){
                App.database.shoppingDao().addItemInShoppingList(article)
            } else{
                App.database.shoppingDao().updateArticle(article.quantity,article.id)
            }
        }
    }
}