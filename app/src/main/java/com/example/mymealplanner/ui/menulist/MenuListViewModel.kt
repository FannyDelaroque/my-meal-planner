package com.example.mymealplanner.ui.menulist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mymealplanner.App
import com.example.mymealplanner.model.*
import java.util.concurrent.Executors


class MenuListViewModel():  ViewModel(){

    val menus: LiveData<List<MenuAndAllItems>> = App.database.menuDao().loadMenuList()


    fun addMenu(menu: Menu){
        Executors.newSingleThreadExecutor().execute {
            App.database.menuDao().insertMenu(menu)
        }
    }

    fun addBreakfastItem(breakfastItem: Item.BreakfastItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertBreakfastItem(breakfastItem)
        }
    }
    fun addLunchItem(lunchItem: Item.LunchItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertLunchItem(lunchItem)
        }
    }
    fun addDinerItem(dinerItem: Item.DinerItem){
        Executors.newSingleThreadExecutor().execute {
            App.database.itemDao().insertDinerItem(dinerItem)
        }
    }

    fun deleteDay(){
        Executors.newSingleThreadExecutor().execute {
            App.database.menuDao().removeMenu()
        }
    }



}