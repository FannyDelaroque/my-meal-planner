package com.example.mymealplanner.ui.favorite

import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.RecipeDetail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.favorite_list.view.*

class FavoriteAdapter (val favoriteList: List<RecipeDetail>,val context: Context, val listener:FavoriteAdapterListener): RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder>() {

    interface FavoriteAdapterListener{
        fun onFavoriteSelected(favoris: RecipeDetail)
        fun onClickButton(favoris: RecipeDetail)
    }

    fun onSelected(position: Int, context: Context){
        listener.onFavoriteSelected(favoriteList[position])
    }

    fun onFavoriteClick(position: Int, context: Context){
        listener.onClickButton(favoriteList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val rootView = LayoutInflater.from(parent.context).inflate(R.layout.favorite_list, parent,false)
        val holder = FavoriteViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
        return favoriteList.size
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val favorite = favoriteList[position]
        if (favorite != null) {
            holder.fillWithFavorite(favorite)
        }
    }


    inner class FavoriteViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener {

        private val image = rootView.imageViewFavorites
        private val title = rootView.textViewTitleFavorite
        private val servingMinute = rootView.textViewServingFavorite
        private val score = rootView.textViewScoreFavorite
        private val container = rootView.containerFavorite
        private val heartbtn = rootView.imageViewHeart

        init {
            container.setOnClickListener(this)
            heartbtn.setOnClickListener(this)
        }

        fun fillWithFavorite(recipeDetail: RecipeDetail) {
            title.text = recipeDetail.title
            val textServing = context.resources.getString(R.string.servingMinute, recipeDetail.servings)
            servingMinute.text = textServing
            val textScore = context.resources.getString(R.string.healthScore, recipeDetail.healthScore)
            score.text = textScore
            Picasso.with(context).load(recipeDetail.image).into(image)

        }

        override fun onClick(v: View) {
            if (v.id != null) {
                when (v.id) {
                    R.id.imageViewHeart -> onFavoriteClick(adapterPosition, v.context)
                    R.id.containerFavorite -> onSelected(adapterPosition, v.context)
                }
            }

        }
    }


}