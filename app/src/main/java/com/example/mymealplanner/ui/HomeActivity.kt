package com.example.mymealplanner.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.example.mymealplanner.R
import com.example.mymealplanner.ui.favorite.FavoriteListFragment
import com.example.mymealplanner.ui.menulist.MenuListFragment
import com.example.mymealplanner.ui.recipelist.RecipeListFragment
import com.example.mymealplanner.ui.shoppinglist.ShoppingListFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {

    lateinit var selectedFragment : Fragment

    companion object{
        private const val ID_MENU_LIST = 1
        private const val ID_GROCERY_LIST = 2
        private const val ID_RECIPE_LIST = 3
        private const val ID_FAVORITE_LIST = 4
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        if(savedInstanceState == null){
            selectedFragment = MenuListFragment()
            val manager = supportFragmentManager
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, selectedFragment)
                .commit()
        }



        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.inflateMenu(R.menu.menu);

        bottomNavigation.add(MeowBottomNavigation.Model(ID_MENU_LIST, R.drawable.ic_view_list_black_24dp))
        bottomNavigation.add(MeowBottomNavigation.Model(ID_GROCERY_LIST, R.drawable.ic_local_grocery_store_black_24dp))
        bottomNavigation.add(MeowBottomNavigation.Model(ID_RECIPE_LIST, R.drawable.ic_restaurant_menu_black_24dp))
        bottomNavigation.add(MeowBottomNavigation.Model(ID_FAVORITE_LIST, R.drawable.ic_favorite_black_24dp))

        bottomNavigation.setOnShowListener {
             when (it.id){
                ID_MENU_LIST -> displayMenuList()
                ID_GROCERY_LIST -> displayGroceryList()
                ID_RECIPE_LIST -> displayRecipeList()
                ID_FAVORITE_LIST -> displayFavoriteList()
                else -> ""
            }
        }
    }



    private fun displayMenuList() {
        selectedFragment = MenuListFragment()
        val manager = supportFragmentManager
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, selectedFragment)
            .commit()
    }

    private fun displayGroceryList(){

        val manager = supportFragmentManager
        selectedFragment = ShoppingListFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, selectedFragment)
            .commit()
    }

    private fun displayRecipeList(){
        val manager = supportFragmentManager
        selectedFragment = RecipeListFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, selectedFragment)
            .commit()
    }

    private fun displayFavoriteList(){
        val manager = supportFragmentManager
        selectedFragment = FavoriteListFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, selectedFragment)
            .commit()
    }



}
