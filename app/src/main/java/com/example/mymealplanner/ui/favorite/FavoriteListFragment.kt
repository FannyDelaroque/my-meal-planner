package com.example.mymealplanner.ui.favorite

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.mymealplanner.R
import com.example.mymealplanner.model.Recipe
import com.example.mymealplanner.model.RecipeAndAllIngredients
import com.example.mymealplanner.model.RecipeDetail
import com.example.mymealplanner.ui.recipedetail.RecipeActivity
import kotlinx.android.synthetic.main.fragment_favorite.view.*

/**
 * A simple [Fragment] subclass.
 */
class FavoriteListFragment : Fragment(), FavoriteAdapter.FavoriteAdapterListener {

    private lateinit var favoriteList: MutableList<RecipeDetail>
    private lateinit var favoriteAdapter: FavoriteAdapter
    private var recyclerView: RecyclerView?= null
    private val favoritesViewModel : FavoriteViewModel by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_favorite, container, false)
            activity?.title = "Mes recettes favorites"
        favoriteList = mutableListOf()
        favoriteAdapter = FavoriteAdapter(favoriteList,this.requireContext(),this)

        recyclerView = view.recycler_favorite
        recyclerView?.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = favoriteAdapter
        }

        //abonnement à la liste de favoris
        favoritesViewModel.favorite.observe(viewLifecycleOwner, Observer { favorites-> updateFavorites(favorites)})
        return view
    }

    private fun updateFavorites(favorites: List<RecipeDetail>) {
        favoriteList.clear()
        for(favorite in favorites){
           favoriteList.add(favorite)
        }

        favoriteAdapter.notifyDataSetChanged()
    }

    override fun onFavoriteSelected(favoris: RecipeDetail) {
        val intent = Intent(this.context, RecipeActivity::class.java)
        intent.putExtra(RecipeActivity.RECIPE_ID, favoris.id)
        intent.putExtra(RecipeActivity.RECIPE_NAME,favoris.title)
        startActivity(intent)
    }

    override fun onClickButton(favoris: RecipeDetail) {
        favoritesViewModel.deleteToFavorite(favoris)
        Toast.makeText(this.requireContext(), "${favoris.title} a été retiré de vos favoris", Toast.LENGTH_SHORT).show()
    }

}
