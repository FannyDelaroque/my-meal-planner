package com.example.mymealplanner.ui.menudetail

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mymealplanner.R
import com.example.mymealplanner.model.Item
import com.example.mymealplanner.model.Item.*
import com.example.mymealplanner.model.Menu
import com.example.mymealplanner.model.MenuAndAllItems
import kotlinx.android.synthetic.main.activity_menu_detail.*
import kotlinx.android.synthetic.main.activity_menu_detail.textViewName


class MenuDetailActivity : AppCompatActivity(), MenuDetailAdapter.MenuDetailAdapterListener, OnEditorActionListener {

    companion object{
        const val ID = "id"
        const val BREAKFAST_ID = "id"
    }

    private lateinit var viewModel: MenuDetailViewModel
    private var menuId = 0

    lateinit var breakfastItemList: MutableList<Item>
    lateinit var lunchItemList: MutableList<Item>
    lateinit var dinerItemList: MutableList<Item>

    private var isBreakfastMenuClicked: Boolean = false
    private var isLunchMenuClicked: Boolean = false
    private var isDinerMenuClicked: Boolean = false

    var currentBreakfastItemNameList: StringBuilder= StringBuilder()
    var currentLunchItemNameList: StringBuilder= StringBuilder()
    var currentDinerItemNameList: StringBuilder= StringBuilder()

    lateinit var currentMenu: Menu


    override fun onResume() {
        super.onResume()
        /**
         * implémentation du viewModel
         */

        val factory = MenuDetailFactory(menuId)
        viewModel = ViewModelProvider(this, factory).get(MenuDetailViewModel::class.java)

        /**
         * abonnement au détail du menu sélectionné
         */
        viewModel.currentMenu.observe(this, Observer { menu -> displayMenu(menu) })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_detail)

        /**
         * hide keyboard
         */
        hideKeyboard()

        /**
         * Récupération des valeurs passées dans l'intent pour le menu en cours
         */
        menuId = intent.getIntExtra(ID, 0)

        /**
         * initialisation des list pour les adapters
         */
        breakfastItemList = mutableListOf()
        lunchItemList = mutableListOf()
        dinerItemList = mutableListOf()

        /**
         * action lorsqu'on entre un aliment dans les editTexts
         * du peti-dejeuner, déjeunerr, diner
         */
        inputBreakfastItem.setOnEditorActionListener(this)
        inputLunchItem.setOnEditorActionListener(this)
        inputDinerItem.setOnEditorActionListener(this)


    }



    fun hideBreakfastItems(view: View) {
        if (!isBreakfastMenuClicked) {
            containerBreakfast.visibility = View.GONE
            btn_arrow_breakfast.setBackgroundResource(R.drawable.ic_add_circle_black_24dp)
            isBreakfastMenuClicked = true
        } else {
            containerBreakfast.visibility = View.VISIBLE
            btn_arrow_breakfast.setBackgroundResource(R.drawable.ic_remove_circle_black_24dp)
            isBreakfastMenuClicked = false
        }
    }

    fun hideLunchItem(view: View){
            if (!isLunchMenuClicked){
                containerLunch.visibility = View.GONE
                btn_arrow_lunch.setBackgroundResource(R.drawable.ic_add_circle_black_24dp)
                isLunchMenuClicked = true
            } else{
                containerLunch.visibility = View.VISIBLE
                btn_arrow_lunch.setBackgroundResource(R.drawable.ic_remove_circle_black_24dp)
                isLunchMenuClicked = false
            }
        }

    fun hideDinerItem(view: View){
            if (!isDinerMenuClicked){
                containerDiner.visibility = View.GONE
                btn_arrow_diner.setBackgroundResource(R.drawable.ic_add_circle_black_24dp)
                isDinerMenuClicked = true
            } else{
                containerDiner.visibility = View.VISIBLE
                btn_arrow_diner.setBackgroundResource(R.drawable.ic_remove_circle_black_24dp)
                isDinerMenuClicked = false
            }
        }

    private fun displayMenu(completeMenu: MenuAndAllItems?) {
        if (completeMenu != null) {

            textViewName.text = completeMenu.menu?.name
            breakfastItemList.clear()
            lunchItemList.clear()
            dinerItemList.clear()



            /***
             * ne pas afficher le recycler si item.name = ""
             */
                 val breakfastList = completeMenu.breakfastItems
                      for (result in breakfastList){
                          if (result.name != ""){
                              breakfastItemList.addAll(listOf(result))
                          }
                      }

            /**
             * afficher la liste d'item pour le petit-déjeuner
             */
            val breakfastAdapter = MenuDetailAdapter(breakfastItemList, this)
            val recyclerBreakfastItem = recyclerBreakfastItem as RecyclerView
            recyclerBreakfastItem!!.layoutManager = LinearLayoutManager(this)
            recyclerBreakfastItem!!.adapter =breakfastAdapter

            /***
             * ne pas afficher le recycler si item.name = ""
             */
            val lunchList = completeMenu.lunchItems
            for (result in lunchList){
                if (result.name != ""){
                    lunchItemList.addAll(listOf(result))
                }
            }

            /**
             * afficher la liste d'item pour le déjeuner
             */
            val lunchAdapter = MenuDetailAdapter(lunchItemList, this)
            val recyclerViewLunchItem = recyclerLunchItem as RecyclerView
            recyclerViewLunchItem!!.layoutManager = LinearLayoutManager(this)
            recyclerViewLunchItem!!.adapter =lunchAdapter

            /***
             * mettre un aliment par ligne dans le recycler
             */
            /***
             * ne pas afficher le recycler si item.name = ""
             */
            val dinerList = completeMenu.dinerItems
            for (result in dinerList){
                if (result.name != ""){
                   dinerItemList.addAll(listOf(result))
                }
            }
            /**
             * afficher la liste d'item pour le diner
             */
            val dinerAdapter = MenuDetailAdapter(dinerItemList, this)
            val recyclerViewDinerItem = recyclerDinerItem as RecyclerView
            recyclerViewDinerItem!!.layoutManager = LinearLayoutManager(this)
            recyclerViewDinerItem!!.adapter = dinerAdapter


        }

    }

   override fun editBreakfastItemSelected(breakfastItem: BreakfastItem) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Modifier l'aliment")
        var editText = EditText(this)
        editText?.setText(breakfastItem.name)
        builder.setView(editText)
        builder.setPositiveButton(R.string.enregistrer) { dialog, which ->
            breakfastItem.name = editText?.text.toString()
            viewModel.updateBreakfastItem(breakfastItem)
            }
        builder.setNegativeButton(R.string.annuler) { dialog, which ->
        }
        builder.show()

    }

    override fun deleteBreakfastItemSelected(breakfastItem: BreakfastItem) {
        viewModel.deleteBreakfastItem(breakfastItem)

    }

    override fun editLunchItemSelected(lunchItem: LunchItem) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Modifier l'aliment")
        var editText = EditText(this)
        editText?.setText(lunchItem.name)
        builder.setView(editText)
        builder.setPositiveButton(R.string.enregistrer) { dialog, which ->
            lunchItem.name = editText?.text.toString()
            viewModel.updateLunchItem(lunchItem)
        }
        builder.setNegativeButton(R.string.annuler) { dialog, which ->
        }
        builder.show()
    }

    override fun deleteLunchItemSelected(lunchItem: LunchItem) {
       viewModel.deleteLunchItem(lunchItem)
    }

    override fun editDinerItemSelected(dinerItem: DinerItem) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Modifier l'aliment")
        var editText = EditText(this)
        editText?.setText(dinerItem.name)
        builder.setView(editText)
        builder.setPositiveButton(R.string.enregistrer) { dialog, which ->
            dinerItem.name = editText?.text.toString()
            viewModel.updateDinerItem(dinerItem)
        }
        builder.setNegativeButton(R.string.annuler) { dialog, which ->
        }
        builder.show()
    }

    override fun deleteDinerItemSelected(dinerItem: DinerItem) {
        viewModel.deleteDinerItem(dinerItem)
    }


    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
         if (actionId == EditorInfo.IME_ACTION_DONE) { //Perform your Actions here.
              when(v?.id){
                  R.id.inputBreakfastItem -> addBreakfastItem(inputBreakfastItem)
                  R.id.inputLunchItem -> addLunchItem(inputLunchItem)
                  R.id.inputDinerItem -> addDinerItem(inputDinerItem)
              }
              return true
          }
         return false
      }

     private fun addBreakfastItem(editText: EditText){
          val input = editText.text.toString()
          val textView :TextView = TextView(this)
          textView.text =input
          val newItem =BreakfastItem()
          newItem.menuId = menuId
          newItem.name = textView.text.toString()
          viewModel.insertBreakfastItem(newItem)
          editText.text.clear()
          hideKeyboard(editText)
      }

      private fun addLunchItem(editText: EditText){
          val input = editText.text.toString()
          val textView :TextView = TextView(this)
          textView.text =input
          val newItem = LunchItem()
          newItem.menuId = menuId
          newItem.name = textView.text.toString()
          viewModel.insertLunchItem(newItem)
          editText.text.clear()
          hideKeyboard(editText)
      }
      private fun addDinerItem(editText: EditText){
          val input = editText.text.toString()
          val textView :TextView = TextView(this)
          textView.text =input
          val newItem = DinerItem()
          newItem.menuId = menuId
          newItem.name = textView.text.toString()
          viewModel.insertDinerItem(newItem)
          currentDinerItemNameList.clear()
          editText.text.clear()
          hideKeyboard(editText)
      }

      private fun hideKeyboard(view: View) {
          view?.apply {
              val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
              imm.hideSoftInputFromWindow(view.windowToken, 0)
          }
      }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

}
