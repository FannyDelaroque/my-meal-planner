package com.example.mymealplanner.helpers

import androidx.recyclerview.widget.RecyclerView

public interface OnStartDragListerner {
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder)
}