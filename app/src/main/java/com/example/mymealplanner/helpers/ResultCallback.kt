package com.example.mymealplanner.helpers

interface ResultCallback {
    fun onPrepare()
    fun onSuccess()
    fun onError()
}