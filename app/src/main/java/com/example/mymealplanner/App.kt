package com.example.mymealplanner

import android.app.Application
import androidx.room.Room
import com.example.mymealplanner.database.DATABASE_NAME
import com.example.mymealplanner.database.Manager
import com.example.mymealplanner.repository.Api
import com.example.mymealplanner.repository.Repository
import com.example.mymealplanner.repository.RetrofitBuilder
import java.util.concurrent.Executors


class App : Application(){

    companion object{
        lateinit var database : Manager
        lateinit var repository: Repository
    }
    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(this, Manager::class.java, DATABASE_NAME)
            .build()
        Executors.newSingleThreadExecutor().execute {
            database.menuDao().loadMenuList()
            database.shoppingDao().loadShoppingList()
        }

        repository = Repository()

    }
}