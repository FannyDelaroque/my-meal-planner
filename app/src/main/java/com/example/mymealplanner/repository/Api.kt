package com.example.mymealplanner.repository

import com.example.mymealplanner.model.*
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @Headers("Content-Type: application/json")
    @GET("recipes/search?query=pasta")
    fun getRandomRecipes(@Query("apiKey") key: String): Call<RecipeList>

    @Headers("Content-Type: application/json")
    @GET("recipes/{id}/information?")
    fun getRecipeById(@Path("id")id: Int,@Query("apiKey") key: String): Call<IngredientList>

    @Headers("Content-Type: application/json")
    @GET("recipes/search")
    fun getSearchRecipes(@Query("query")query:String,@Query("apiKey") key: String): Call<RecipeList>

}