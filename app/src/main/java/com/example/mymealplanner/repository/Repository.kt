package com.example.mymealplanner.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.mymealplanner.helpers.ResultCallback
import com.example.mymealplanner.model.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Query


const val API_KEY = "750879c9fdba4756b1a9821e3a565c28"

class Repository() {
    private val service = RetrofitBuilder.buildService(Api::class.java)

    fun getRandomRecipes(callback: ResultCallback,resultHandler:(recipelist: RecipeList, error:String?)->Unit){
        var recipeList: RecipeList

        service.getRandomRecipes(API_KEY).enqueue(object : Callback<RecipeList> {
            override fun onFailure(call: Call<RecipeList>, t: Throwable) {
            Log.d("Repository", t.message.toString())
                callback.onError()
            }

            override fun onResponse(call: Call<RecipeList>, response: Response<RecipeList>) {
                if (response.isSuccessful){
                    recipeList = response.body()!!
                    resultHandler(recipeList,null)
                    callback.onSuccess()

                }
                Log.d("RepositoryOnResponse", response.body().toString())

            }
        })
    }
    fun getSearchRecipe(callback: ResultCallback,query:String,resultHandler:(recipelist: RecipeList, error:String?)->Unit){
        var recipeList: RecipeList

        service.getSearchRecipes(query,API_KEY).enqueue(object : Callback<RecipeList> {
            override fun onFailure(call: Call<RecipeList>, t: Throwable) {
                Log.d("Repository", t.message.toString())
                callback.onError()
            }

            override fun onResponse(call: Call<RecipeList>, response: Response<RecipeList>) {
                if (response.isSuccessful){
                    recipeList = response.body()!!
                    resultHandler(recipeList,null)

                }
                Log.d("RepositoryOnResponse", response.body().toString())
                callback.onSuccess()
            }
        })
    }

    fun getRecipeById(callback: ResultCallback,id: Int,resultHandler:(recipe: IngredientList, error:String?)->Unit){
        var recipe: IngredientList
        service.getRecipeById(id, API_KEY).enqueue(object : Callback<IngredientList>{
            override fun onFailure(call: Call<IngredientList>, t: Throwable) {
                Log.d("RepositoryFail", t.message.toString())
                callback.onError()
            }

            override fun onResponse(call: Call<IngredientList>, response: Response<IngredientList>) {
                if(response.isSuccessful){
                    recipe = response.body()!!
                    resultHandler(recipe!!,null)
                    callback.onSuccess()
                }
                Log.d("RepositoryGetRecipeByID", response.body().toString())
            }

        })
    }



}