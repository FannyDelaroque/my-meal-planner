package com.example.mymealplanner.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.mymealplanner.model.RecipeAndAllIngredients
import com.example.mymealplanner.model.RecipeDetail

@Dao
interface FavoriteDao {

        /**
         * Usage:
         * dao.insertFavorite(show)
         */

        @Insert
        fun insertFavorite(recipeDetail: RecipeDetail)

        /**
         * Usage:
         * dao.deleteFavorite(favorite)
         */
        @Delete
        fun deleteFavorite(recipeDetail: RecipeDetail)

        /**
         * dao.getAllFavorites().observe(this, Observer{ favorite -> ...}
         * avec le liveData on s'abonne au changement
         */
        @Query("SELECT * FROM recipe")
        fun getAllFavorites(): LiveData<List<RecipeDetail>>

        /**
         * dao.getFavoriteById(3).observe(this, Observer{ favorite -> ...}
         */
        @Query("SELECT *FROM recipe WHERE id= :recipeId")
        fun getFavoritesById(recipeId: Int): LiveData<RecipeDetail>





}