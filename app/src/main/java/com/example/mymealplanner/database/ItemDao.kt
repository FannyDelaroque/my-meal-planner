package com.example.mymealplanner.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mymealplanner.model.Item.*


@Dao
interface ItemDao {


    /**
     * avoir un breakfastItem par son id
     */
    @Query("SELECT * FROM breakfast_item WHERE breakfast_id=:id")
    fun getBreakfastItemById(id: Int): LiveData<BreakfastItem>

    /**
     * update itemBreakfast
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateBreakfastItem(breakfastItem: BreakfastItem)

    /**
     * avoir un LunchItem par son id
     */
    @Query("SELECT * FROM lunch_item WHERE lunch_id=:id")
    fun getLunchItemById(id: Int): LiveData<LunchItem>

    /**
     * update lunchItem
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateLunchItem(lunchItem: LunchItem)

    /**
     * avoir un dinerItem par son id
     */
    @Query("SELECT * FROM diner_item WHERE diner_id=:id")
    fun getDinerItemById(id: Int): LiveData<DinerItem>

    /**
     * update itemBreakfast
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateDinerItem(dinerItem: DinerItem)

    /**
     * insérer un nouveau breakfastItem
     */
    @Insert
    fun insertBreakfastItem(breakfastItem: BreakfastItem)

    /**
     * insérer un nouveau lunchItem
     */
    @Insert
    fun insertLunchItem(lunchItem: LunchItem)

    /**
     * insérer un nouveau dinerItem
     */
    @Insert
    fun insertDinerItem(dinerItem: DinerItem)

    /**
     * delete breakfastItem
     */
    @Delete
    fun deleteBreakfastItem(breakfastItem: BreakfastItem)

    /**
     * delete lunchItem
     */
    @Delete
    fun deleteLunchItem(lunchItem: LunchItem)

    /**
     * delete dinerItem
     */
    @Delete
    fun deleteDinerItem(dinerItem: DinerItem)
}