package com.example.mymealplanner.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mymealplanner.model.ShoppingList

@Dao
interface ShoppingListDao {

    /**
     * charger la liste de course
     */
    @Query("SELECT * FROM shopping_list")
        fun loadShoppingList(): LiveData<List<ShoppingList>>

    /**
     * insérer un aliment dans la liste de course
     */
    @Insert
    fun addItemInShoppingList(item: ShoppingList)

    /**
     * supprimer un article de la liste de course
     */
    @Delete
    fun removeItemFromShoppingList(item: ShoppingList)

    /**
     * mettre à jour un article de la liste de course
     */
    @Update(onConflict =OnConflictStrategy.REPLACE)
    fun updateItem(item: ShoppingList)

    @Query("UPDATE shopping_list SET quantity=:quantity WHERE id=:id")
    fun updateArticle(quantity: Int, id:Int)

    /**
     * update id after drag & drop
     */
    @Query("INSERT INTO shopping_list (id,name,quantity) VALUES (:newId, :currentName, :currentQuantity)")
        fun updateArticleId(newId:Int, currentName:String, currentQuantity:Int)


    /**
     * avoir un article par son id
     */
    @Query("SELECT * FROM shopping_list WHERE id=:id")
    fun getArticleById(id: Int): LiveData<ShoppingList>


}