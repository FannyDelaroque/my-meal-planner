package com.example.mymealplanner.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mymealplanner.model.*

const val DATABASE_NAME = "menus"

@Database(entities = [Menu::class, Item.BreakfastItem::class, Item.LunchItem::class, Item.DinerItem::class, ShoppingList::class, RecipeDetail::class, ExtendedIngredient::class], version = 1)
// abstract car Room génère le contenu
abstract class Manager: RoomDatabase() {
    abstract fun menuDao() : MenuDao
    abstract fun itemDao(): ItemDao
    abstract fun shoppingDao() : ShoppingListDao
    abstract fun favoriteDao(): FavoriteDao
}