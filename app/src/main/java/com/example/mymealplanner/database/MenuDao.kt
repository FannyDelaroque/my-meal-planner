package com.example.mymealplanner.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mymealplanner.model.Item.*
import com.example.mymealplanner.model.Menu
import com.example.mymealplanner.model.MenuAndAllItems

@Dao
interface MenuDao {

    /**
     * avoir un menu par son id
     */
    @Transaction
    @Query("SELECT *FROM menu WHERE id=:id")
    fun getMenuById(id: Int): LiveData<MenuAndAllItems>


    /**
     * enregistrer un menu
     */
        @Insert
        fun insertMenu(menu: Menu)



    /**
     * chargement de tous les menus
     */

    @Transaction
    @Query("SELECT * FROM menu ORDER BY id ")
    fun loadMenuList(): LiveData<List<MenuAndAllItems>>



    /**
     * update d'un menu
     */
    @Transaction
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateMenu(menu: Menu): Int



    /***
     * supprimer un menu
     */
    @Query("DELETE FROM menu WHERE id in(SELECT MAX(id) FROM menu) ")
    fun removeMenu()


}